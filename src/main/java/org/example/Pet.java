package org.example;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String x, String y){
        species = x;
        nickname = y;
    }
    public Pet(String x, String y, int i, int k, String[] hab){
        species = x;
        nickname = y;
        age = i;
        trickLevel = k;
        habits = hab;
    }

    public Pet(){};

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits(){
        return habits;
    }

    public void setHabits(String[] habits){
        this.habits = habits;
    }


    public void eat() {
        System.out.println("Я їм!");
    }

    public void respond() {
        System.out.println("Привіт, хазяїн. Я - " + this.nickname + ". Я скучив!");
    }

    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }
}
