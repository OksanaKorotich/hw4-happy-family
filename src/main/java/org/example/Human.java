package org.example;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;

    //methods

    public void greetPet() {
        System.out.println("Привіт, " + this.pet.getNickname());
    }

    public String describePet() {
        String trickLevelDescription = this.pet.getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";
        return "У мене є " + this.pet.getSpecies() + ", їй " + this.pet.getAge() + " років, він " + trickLevelDescription + ".";
    }

   // constructors

    public Human(){};

    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human father, Human mother, int iq, Pet pet, String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.father = father;
        this.mother = mother;
        this.pet = pet;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, Human father, Human mother){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.father = father;
        this.mother = mother;
    }

    //getters setters

    public String getName(){return name;}
    public void setName(String name){this.name = name;}

    public String getSurname(){return surname;}
    public void setSurname(String surname){this.surname = surname;}

    public int getYear(){return year;}
    public void setYear(int year){this.year = year;}

    public int getIq(){return iq;}
    public void setIq(int iq){this.iq = iq;}

    public Pet getPet(){return pet;}
    public void setPet(Pet pet){this.pet = pet;}

    public Human getMother(){return mother;}
    public void setMother(Human mother){this.mother = mother;}

    public Human getFather(){return father;}
    public void setFather(Human father){this.father = father;}

    public String[][] getSchedule(){return schedule;}
    public void setSchedule(String[][] schedule){this.schedule= schedule;}

}
