package org.example;

public class Main {
    public static void main(String[] args) {
        Pet cat = new Pet("cat", "Sanches");
        cat.eat();
        cat.respond();
        cat.foul();

        Human mama = new Human("Oksana", "Korotich", 1980);
        mama.setPet(cat);
        mama.greetPet();

        System.out.println(cat.getNickname());
        System.out.println(cat.getSpecies());
        System.out.println(mama.getName());
    }
}